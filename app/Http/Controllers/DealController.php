<?php

namespace App\Http\Controllers;

use App\Models\Deal;
use Illuminate\Http\Request;

class DealController extends Controller
{
    public function create(Request $request)
    {
        Deal::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);
        return 1;
    }
    public function show()
    {
        $Deals = Deal::all();
        return response()->json([
            $Deals,
        ]);
    }
    public function byId(Request $request)
    {
        $Deal = Deal::find($request->id);
        return response()->json([
            $Deal,
        ]);
    }
    public function done(Request $request)
    {
        $Deal = Deal::find($request->id);
        if (!$Deal->done)
        {
            $Deal->fill(['done' => 1])->save();
            return 1;
        }
        else
        {
            return "already done";
        }
    }
    public function undone(Request $request)
    {
        $Deal = Deal::find($request->id);
        if ($Deal->done)
        {
            $Deal->fill(['done' => 0])->save();
            return 1;
        }
        else
        {
            return "already undone";
        }
    }
}
