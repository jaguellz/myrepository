<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('new', 'DealController@create');
Route::get('deal', 'DealController@byID');
Route::get('deals', 'DealController@show');
Route::get('done', 'DealController@done');
Route::get('undone', 'DealController@undone');
